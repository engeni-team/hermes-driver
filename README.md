# Engeni Hermes Email Driver

Engeni's Hermes Email Driver is a email driver to be able to send emails throught the Hermes service using the Laravel
Mail feature.

Please note that you need an Engeni API Key to access Hermes.

**Please read the docs <https://engeni.atlassian.net/wiki/spaces/HRMS>**

## Installation

Install package with composer:

```bash
composer require engeni/hermes-driver
```

## Configuration

### Step 1: Config .env

In the .env file, wrtie the following variables and the values that apply to your development environemnt.
These are just examples:

```php
MAIL_MAILER="hermes"
```

Then, also in the .env file, add the following:

```php
########### BEGIN ENGENI VARS ###########
### ...
## Engeni API Hermes
ENGENI_HERMES_API_URL="${ENGENI_API_GATEWAY_URL}/hrms"
ENGENI_HERMES_API_KEY="xGjRHQkVhIHccYSXyp7C5P2AZngCyMaK"

########### END ENGENI VARS ###########
```

### Step 2: Config App

In config/app.php add the following provider:

```php
'providers' => [
    //...
    // Illuminate\Mail\MailServiceProvider::class,
    \Engeni\HermesDriver\HermesMailServiceProvider::class,
],
```

### Step 3: Hermes Driver Config

Add the following information to your `config/engeni.php` file in the Laravel config folder:

```php
return [
    // ...

    'hermes' => [
        'api' => [
            'url' => env('ENGENI_HERMES_API_URL', env('ENGENI_API_GATEWAY_URL') .'/hrms'),
            'key' => env('ENGENI_HERMES_API_KEY'),
            'ssl' => true
        ],
        'options' => [
            'service_id' => env('SERVICE_ID'),
            'reply_to' => env('ENGENI_HERMES_REPLY_TO'),
            'reply_to_name' => env('ENGENI_HERMES_REPLY_TO_NAME')
        ]
    ]
]
```

You can find the code above in ./src/engeni.php

### Step 4: Laravel Mail Config

In mail.php config add:

```php
'hermes' => [
    'transport' => 'hermes',
],
```

## Example

The Hermes Drive works exactly as any other Laravel Email Driver.

Create a Mailable:

```bash
php artisan make:mail OrderShipped
```

This is the Mailable class recently created:

```php
<?php
 
namespace App\Mail;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
 
class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;
 
    public $data;
 
    public function __construct(array $data): void
    {
        $this->data = $data;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.order-shipped');
    }
}
```

Note: You will have to create a Blade Template, such as /resources/views/emails/order-shipped.blade.php:

```html
<div>
Data: {{ print_r($data) }}
</div>
```

Then send the email:

```php
use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;

Mail::to($request->user())
    ->cc($moreUsers)
    ->bcc($evenMoreUsers)
    ->send(new OrderShipped($data));
```

See <https://laravel.com/docs/9.x/mail#sending-mail> for more information.
