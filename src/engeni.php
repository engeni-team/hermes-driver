<?php

return [
    'hermes' => [
        'api' => [
            'url' => env('ENGENI_HERMES_API_URL', env('ENGENI_API_GATEWAY_URL').'/hrms'),
            'key' => env('ENGENI_HERMES_API_KEY'),
            'ssl' => true,
        ],
        'options' => [
            'service_id' => env('SERVICE_ID'),
            'reply_to' => env('ENGENI_HERMES_REPLY_TO'),
            'reply_to_name' => env('ENGENI_HERMES_REPLY_TO_NAME'),
        ],
    ],
];
