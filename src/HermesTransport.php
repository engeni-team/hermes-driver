<?php

namespace Engeni\HermesDriver;

use GuzzleHttp\ClientInterface;
use Illuminate\Support\Fluent;
use Symfony\Component\Mailer\SentMessage;
use Symfony\Component\Mailer\Transport\AbstractTransport;
use Symfony\Component\Mime\MessageConverter;

class HermesTransport extends AbstractTransport
{
    /**
     * Guzzle client instance.
     *
     * @var \GuzzleHttp\ClientInterface
     */
    protected $client;

    /**
     * API key.
     *
     * @var string
     */
    protected $key;

    /**
     * The API URL to which to POST emails.
     *
     * @var string
     */
    protected $url;

    /**
     * Hermes options.
     *
     * @var string
     */
    protected $options;

    /**
     * Create a new Custom transport instance.
     *
     * @param \GuzzleHttp\ClientInterface $client
     * @param string|null $url
     * @param string $key
     * @return void
     */
    public function __construct(ClientInterface $client, $url, $key, array $options = [])
    {
        parent::__construct();

        $this->key = $key;
        $this->client = $client;
        $this->url = $url;
        $this->options = $options;
    }

    /**
     * {@inheritdoc}
     */
    protected function doSend(SentMessage $message): void
    {
        $payload = $this->getPayload($message);
        $this->client->request('POST', $this->getSendMessageUrl(), $payload);
    }

    /**
     * Get the HTTP payload for sending the message.
     *
     * @param \Swift_Mime_SimpleMessage $message
     * @return array
     */
    protected function getPayload(SentMessage $message)
    {
        $metadata = $this->getMetadata($message);
        $message = MessageConverter::toEmail($message->getOriginalMessage());
        $from = isset($message->getFrom()[0]) ? $message->getFrom()[0]->getAddress() : null;
        $fromName = isset($message->getFrom()[0]) ? $message->getFrom()[0]->getName() : null;
        $to = isset($message->getTo()[0]) ? $message->getTo()[0]->getAddress() : null;
        $toName = isset($message->getTo()[0]) ? $message->getTo()[0]->getName() : null;
        $replayTo = isset($message->getReplyTo()[0]) ? $message->getReplyTo()[0]->getAddress() : null;
        $replayToName = isset($message->getReplyTo()[0]) ? $message->getReplyTo()[0]->getName() : null;

        return [
            'headers' => [
                'X-Api-Token' => $this->key,
                'Accept' => 'application/json',
            ],
            'json' => [
                'type' => 'email',
                'service_id' => $this->options['service_id'],
                'topic' => $metadata->topic,
                'account_id' => $metadata->account_id,
                'order_id' => $metadata->order_id,
                'payload' => [
                    'from' => $from ?? $this->getDefaultFrom(),
                    'from_name' => $fromName ?? $this->getDefaultFromName(),
                    'to' => $to,
                    'to_name' => $toName,
                    'reply_to' => $replayTo ?? $this->getDefaultReplyTo(),
                    'reply_to_name' => $replayToName ?? $this->getDefaultReplyToName(),
                    'subject' => $message->getSubject(),
                    'html_body' => $message->getHtmlBody() ?? $message->getTextBody(),
                ],
            ],
        ];
    }

    protected function getMetadata(SentMessage $message): Fluent
    {
        try {
            return new Fluent(collect($message->getOriginalMessage()->getHeaders()->all())->filter(function ($value, $key) {
                return str_contains($key, 'metadata');
            })->flatMap(function ($value, $key) {
                return [strtolower(str_replace('x-metadata-', '', $key)) => $value->getValue()];
            }));
        } catch (\Throwable $exception) {
            \Log::error("Can't obtain email metadata.");
            \Log::error($exception->getMessage());
            return new Fluent();
        }
    }

    /**
     * Get create messages endpoint.
     *
     * @return string
     */
    protected function getSendMessageUrl(): string
    {
        return $this->url . '/messages';
    }

    protected function getDefaultFrom(): ?string
    {
        return config('mail.from.address');
    }

    protected function getDefaultFromName(): ?string
    {
        return config('mail.from.name');
    }

    protected function getDefaultReplyTo(): ?string
    {
        return config('mail.reply_to.address');
    }

    protected function getDefaultReplyToName(): ?string
    {
        return config('mail.reply_to.name');
    }

    /**
     * Get the string representation of the transport.
     *
     * @return string
     */
    public function __toString(): string
    {
        return 'hermes';
    }
}
