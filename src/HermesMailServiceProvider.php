<?php

namespace Engeni\HermesDriver;

use GuzzleHttp\Client as HttpClient;
use Illuminate\Mail\MailServiceProvider;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Mail;

class HermesMailServiceProvider extends MailServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Mail::extend('hermes', function (array $config = []) {
            $config = $this->app['config']->get('engeni.hermes.api', []);
            $options = $this->app['config']->get('engeni.hermes.options', []);
            $config['guzzle']['verify'] = $config['ssl'] ?? true;

            return new HermesTransport($this->guzzle($config), $config['url'], $config['key'], $options);
        });
    }

    /**
     * Get a fresh Guzzle HTTP client instance.
     *
     * @param  array  $config
     * @return \GuzzleHttp\Client
     */
    protected function guzzle(array $config)
    {
        return new HttpClient(Arr::add(
            $config['guzzle'] ?? [],
            'connect_timeout',
            60
        ));
    }
}
